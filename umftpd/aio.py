"""Asyncio utilities."""

import asyncio
import collections.abc
import concurrent.futures
import threading
import typing

T = typing.TypeVar('T')

RESOLVED = concurrent.futures.Future()
RESOLVED.set_result(None)


class AIOLoopThread(threading.Thread):
    def __init__(self) -> None:
        self.loop = asyncio.new_event_loop()
        super().__init__(name=type(self).__name__, daemon=True)

    def run(self) -> None:
        asyncio.set_event_loop(self.loop)
        self.loop.run_forever()
        self.loop.run_until_complete(asyncio.gather(
            *(task for task in asyncio.all_tasks(self.loop) if task.cancel()),
            return_exceptions=True,
            ))
        self.loop.close()

    def submit(
            self,
            coro: collections.abc.Coroutine[T],
            ) -> concurrent.futures.Future[T]:
        return asyncio.run_coroutine_threadsafe(coro, self.loop)

    def cancel(self, future: concurrent.futures.Future) -> None:
        self.loop.call_soon_threadsafe(future.cancel)

    def stop(self, timeout: float | None = None) -> None:
        self.loop.call_soon_threadsafe(self.loop.stop)
        self.join(timeout)
