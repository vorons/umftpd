"""User interface micro-toolkit.

It is 2023 and yet some weirdos are still trying to push everybody into
writing XML while the only alternatives are an unmaintained GUI designer and
C-like imperative code, just insane.

This module provides just a bunch of classes and functions enabling declarative
interfaces using the standard APIs with a custom GObject factory function,
so we can just declare, organize and nest widgets in a sensible way.

"""
import collections
import collections.abc
import contextlib
import contextvars
import dataclasses
import functools
import importlib.resources
import importlib.util
import json
import pathlib
import types
import typing
import weakref

import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Adw, Gdk, GLib, Gio, GObject, Gtk

import ustache as mstache

import umftpd.uitk.mlogic as mlogic

V = typing.TypeVar('V')
G = typing.TypeVar('G', bound=GObject.Object)
R = typing.TypeVar('R', bound=Gtk.Root | Gtk.NativeDialog | None)
H = typing.TypeVar('H', bound=typing.Callable[..., bool | None])

file_not_found_error_suppressor = contextlib.suppress(FileNotFoundError)
key_error_supressor = contextlib.suppress(KeyError)


class DuplicatedKeyError(KeyError):
    """Key error due duplicated key in unique-key mappings."""


class DuplicatedWidgetNameError(KeyError):
    """Widget error due duplicated name in widget mappings."""


class DuplicatedDomainError(KeyError):
    """Domain error due duplicated domain name."""


class RootCollisionError(ValueError):
    """Widget error due its root being different to widget mapping root."""


@dataclasses.dataclass(frozen=True)
class Translation:
    code: str
    translators: 'collections.abc.Sequence[str]'
    data: 'collections.abc.Mapping[str, str]'


class UniqueAttrDict(collections.UserDict[str, V]):
    """AttrDict ensuring existing keys don't get overwritten."""

    def __setitem__(self, key: str, item: V) -> None:
        if self.data.setdefault(key, item) is not item:
            raise DuplicatedKeyError(key)

    def __getattr__(self, key: str) -> V:
        try:
            return self[key]
        except KeyError as e:
            raise AttributeError(key) from e


class StartupManager:
    """Base class for application startup callbacks."""

    _all = weakref.WeakValueDictionary[int, 'StartupManager']()
    _application: Gtk.Application | None = None
    _demoable = True

    _on_bind: 'collections.abc.Iterable[typing.Callable[[], typing.Any]]'

    def __init__(self) -> None:
        super().__init__()
        self._all[id(self)] = self
        self._on_bind = []

    def bind(self, application: Gtk.Application) -> None:
        self._application = application

        if self._demoable or not isinstance(application, UiDemoAplication):
            # TODO: connect startup? self._application.connect('')
            self._on_bind, on_bind = (), self._on_bind
            for callback in on_bind:
                callback()

    @classmethod
    def bind_all(cls, application: Gtk.Application) -> None:
        cls._application = application
        for self in cls._all.values():
            self.bind(application)


class EventManager(StartupManager, typing.Generic[R]):
    """Deferred Gtk connection manager for both events and actions."""

    _root: R | None = None
    _bound = weakref.WeakKeyDictionary()
    _demoable = False

    @property
    def root(self) -> R:
        return self._root or self._application

    def __init__(self, root: R = None) -> None:
        super().__init__()
        self._root = root
        self._holders = []

    def resolve(self, name: str) -> R | Gio.Action:
        if name is None:
            return self.root
        return self.root.lookup_action(name)

    def connect(
            self,
            name_or_signal: str | None,
            signal_or_handler: H | str | None = None,
            handler: H | None = None,
            *_args: typing.Any,
            args: 'collections.abc.Iterable' = (),
            ) -> H | typing.Callable[[H], H]:
        name, signal, handler, *args = (
            (name_or_signal, signal_or_handler, handler, *_args, *args)
            if isinstance(signal_or_handler, str) else
            (None, name_or_signal, signal_or_handler, *_args, *args)
            )

        if signal is None:
            message = 'a signal must be provided'
            raise ValueError(message)

        if handler is None:
            return functools.partial(self.connect, name, signal, args=args)

        if self._application:
            handler = self._bound[handler] or handler
            self.resolve(name).connect(signal, handler, *args)
            return handler

        self._bound.setdefault(handler)
        self._on_bind.append(
            functools.partial(self.connect, name, signal, handler, args=args),
            )
        return handler

    def bind_handlers(self, obj: object) -> None:
        for name, handler in vars(type(obj)).items():
            if callable(handler) and handler in self._bound:
                method = getattr(obj, name, None)
                if callable(method):
                    self._bound[handler] = method

    def bind(self, app: Gtk.Application) -> None:
        self.bind_handlers(app)
        super().bind(app)


class WidgetManager(EventManager[R], UniqueAttrDict[Gtk.Widget]):
    """GtkWidget manager with context-based widget registry."""

    _ctx = contextvars.ContextVar['WidgetManager']('WidgetDictContext')
    _token: contextvars.Token['WidgetManager'] | None = None

    def __init__(self, *nodes: Gtk.Widget) -> None:
        super().__init__()
        self.register(nodes)

    def __setitem__(self, key: str, item: Gtk.Widget) -> None:
        try:
            super().__setitem__(key, item)
        except KeyError:
            raise DuplicatedWidgetNameError(key) from None

    def __enter__(self) -> 'WidgetManager[R]':
        if self._token:
            message = f'{type(self).__name__} context collision'
            raise RuntimeError(message)
        self._token = self._ctx.set(self)
        return self

    def __exit__(self, *_) -> None:
        self._ctx.reset(self._token)

    def resolve(self, name: str) -> Gtk.Widget | Gio.Action | R:
        return super().resolve(name) or self[name]

    def register(self, nodes: 'collections.abc.Iterable') -> None:
        root = next(
            (value for value in nodes if isinstance(value, Gtk.Root)),
            None,
            )
        if root:
            if self._root not in (root, None):
                raise RootCollisionError(root, self._root)

            self._root = root

        self.update(
            (name, widget)
            for name, widget in ((self._name(v), v) for v in nodes)
            if name
            )

    @classmethod
    def _name(cls, value: typing.Any) -> str | None:
        if isinstance(value, Gtk.Widget):
            wcls = type(value)
            name = value.get_name()
            qualname = f'{wcls.__module__}{wcls.__name__}'
            if name and not qualname.endswith(f'.{name}'):
                return name
        return None

    @classmethod
    def _register(cls, nodes: 'collections.abc.Iterable') -> None:
        self = cls._ctx.get(None)
        if self is not None:
            self.register(nodes)

    def bind(self, application: Gtk.Application) -> None:
        if isinstance(self._root, Gtk.Window):
            application.connect('startup', self._root.set_application)
        super().bind(application)


class IconManager(StartupManager):
    """Importlib-based Gtk icon resource manager."""

    _theme: Gtk.IconTheme | None = None

    def __init__(
            self,
            namespaces: 'collections.abc.Iterable[str]' = (),
            ) -> None:
        """Initialize."""
        super().__init__()
        for namespace in namespaces:
            self.add_namespace(namespace)

    def add_namespace(self, namespace: str) -> None:
        """
        Register module namespace on default Gtk.IconTheme.

        `Gtk` requires icon resources to be on a `hicolor/{size}/{category}/`
        directory structure inside the namespace.

        """
        if self._theme is None:
            self._on_bind.append(functools.partial(
                self.add_namespace,
                namespace,
                ))
            return

        # filesystem case, register namespace path
        spec = importlib.util.find_spec(namespace)
        search_locations = spec.submodule_search_locations if spec else ()
        if search_locations:
            for path in search_locations:
                self._theme.add_search_path(path)
            return

        # virtual, cache tree structure
        directory = self._application.get_application_id() or namespace
        cache = pathlib.Path(GLib.get_user_cache_dir(), directory, 'icons')
        root = importlib.resources.files(namespace)
        queue = collections.deque([(cache, root)])
        while queue:
            dest, directory = queue.popleft()
            dest.mkdir(parents=True, exist_ok=True)
            for traversable in directory.iterdir():
                name = traversable.name
                if traversable.is_file():
                    (dest / name).write_bytes(traversable.read_bytes())
                elif traversable.is_dir():
                    queue.append((dest / name, traversable))
        self._theme.add_search_path(str(cache))

    def bind(self, application: Gtk.Application) -> None:
        self._theme = Gtk.IconTheme.get_for_display(Gdk.Display.get_default())
        return super().bind(application)


class MessageManager(UniqueAttrDict[str]):
    """Makeshift translatable message manager."""

    _all = weakref.WeakValueDictionary[str, 'MessageManager']()
    _localized: dict[str, str] = types.MappingProxyType({})
    _template = types.MappingProxyType({
        'translators': [
            'Translator Name <optional@email.here>',
            ],
        })

    @functools.cached_property
    def _translations(self) -> tuple[Translation, ...]:
        return tuple(map(self._translation, sorted(
            traversable.name[:-5]
            for traversable in (
                self._namespace.iterdir()
                if self._namespace else
                ()
                )
            if traversable.name.endswith('.json')
            )))

    def __init__(
            self,
            domain: str = 'global',
            namespace: str | None = None,
            *args, **fields,
            ) -> None:
        super().__init__(*args, **fields)

        if self._all.get(domain, self).data != self.data:
            raise DuplicatedDomainError(domain)

        locales = GLib.get_language_names()
        self._domain = domain
        self._namespace = (
            importlib.resources.files(namespace)
            if namespace else
            None
            )
        self._locales = sorted(
            {k[:-6] if k.endswith('.UTF-8') else k for k in locales} - {'C'},
            key=locales.index,
            )

        translation = self._translation(*reversed(self._locales))
        self._translators = translation.translators
        self._localized = translation.data
        self._all[domain] = self

    def render(self, key: str, **kwargs) -> str:
        return mstache.render(
            self[key],
            kwargs,
            resolver=self.get,
            getter=mlogic.getter,
            lambda_render=mlogic.lambda_render,
            )

    def _translation(self, *locales) -> Translation:
        code = 'C'
        merged_translators = []
        merged_translation = {}
        if self._namespace:
            for locale in locales:
                try:
                    data = json.loads(
                        (self._namespace / f'{locale}.json').read_text(),
                        )
                except FileNotFoundError:
                    continue

                translation = {
                    key: value['string']
                    for key, value in data.get(self._domain, {}).items()
                    if not key.startswith('_')
                    }
                if not translation:
                    continue

                code = locale
                translators = data.get('translators', ())
                if frozenset(translation).issuperset(merged_translation):
                    merged_translators[:] = translators
                else:
                    merged_translators.extend(translators)
                merged_translation.update(translation)

        return Translation(
            code=code,
            translators=tuple(sorted(
                frozenset(merged_translators),
                key=merged_translators.index,
                )),
            data=types.MappingProxyType(merged_translation),
            )

    def __getitem__(self, key: str) -> Gtk.Widget:
        for source in (self._localized, self.data):
            with key_error_supressor:
                return source[key]
        if any(map(key.startswith, ('render_', '_render_'))):
            return functools.partial(self.render, key[7:])
        raise AttributeError(key)

    @classmethod
    def all_translators(cls) -> list[str]:
        translators = [
            translator
            for _, translator in sorted(
                (index, translator)
                for manager in cls._all.values()
                for index, translator in enumerate(manager._translators)
                )
            ]
        return sorted(frozenset(translators), key=translators.index)

    @classmethod
    def _render_all(cls, template: str) -> str:
        return mstache.render(template, cls._all, resolver=cls._all.get)

    @classmethod
    def _dump_all(cls) -> dict[str, dict[str, dict[str, str]]]:
        return cls._template | {
            domain: {
                key: {'string': value}
                for key, value in manager.data.items()
                if not key.startswith('_')
                }
            for domain, manager in cls._all.items()
            }


class UiApplication(Adw.Application):
    """Base application class with StartupManager initalization."""

    actions: 'collections.abc.Iterable[Gio.Action]' = ()

    def __init__(self, *args, **kwargs) -> None:
        """Initialize."""
        super().__init__(*args, **kwargs)
        for action in self.actions:
            self.add_action(action)
        StartupManager.bind_all(self)


class UiDemoAplication(UiApplication):
    """Application showing every knwon top-level window."""

    def do_activate(self) -> None:
        """Show every known window."""
        for manager in StartupManager._all.values():
            root = getattr(manager, 'root', None)
            if root:
                if isinstance(root, Gtk.NativeDialog):
                    root.show()
                elif isinstance(root, Gtk.Window):
                    root.set_application(self)
                    root.add_css_class('devel')
                    root.show()


def create(cls: type[G], *args, **kwargs) -> G:
    """
    Create instance of given GObject, with extra parameter handling.

    Parameters will be applied as properties in the constructor, if possible,
    otherwise they will be set via matching methods (see notes below).

    Notes
    =====

    If positional arguments are provided, `cls.new` method will be used
    if available (so keyword will only include method calls),
    `cls` will be used otherwise.

    This is how keyword arguments are processed:

    - ``{name}``:
      Call ``widget.[set_]{name}(value)``.
    - ``{name}_apply``:
      Call ``widget.[set_]{name}(value(widget))``.
      Other modifiers can be chained before ``_apply``.
    - ``{name}_on_{property}``:
      Call ``widget.get_{property}().[set_]{name}(value)``,
      but only if widget.get_{property}() returns non-falsy value.
      Other modifiers (this included) can be chained before ``_on_``.
    - ``{name}_many``:
      Loop value calling ``widget.[set_]{name}(value[i])``.
    - ``{name}_args``:
      Call ``widget.[set_]{name}(*value)``.
    - ``{name}_kwargs``:
      Call ``widget.[set_]{name}(**value)``.
    - ``{name}_many_args``:
      Loop value calling ``widget.[set_]{name}(*value[i])``.
    - ``{name}_many_kwargs``:
      Loop value calling ``widget.[set_]{name}(**value[i])``.

    """
    register = WidgetManager._register

    if args:
        self = getattr(cls, 'new', cls)(*args)
        register(args)
    else:
        properties = {
            key: kwargs.pop(key)
            for key in (
                spec.name.replace('-', '_')
                for spec in GObject.ObjectClass.list_properties(cls)
                )
            if key in kwargs
            }
        self = cls(**properties)
        register(properties.values())

    for pair in kwargs.items():
        key, value, obj = *pair, self

        if key.endswith('_apply'):
            # add_apply -> add(value(widget))
            key, value = key[:-6], value(self)

        while obj and '_on_' in key and not hasattr(obj, key):
            # add_on_child -> if get_child(): get_child().add(...)
            key, prop = key.rsplit('_on_', 1)
            obj = getattr(obj, f'get_{prop}')()

        if not obj:
            # TODO: warning
            continue

        key, calls = (
            (
                # add_many_args -> add(*value[0]), add(*value[1]), ...
                (key[:-10], [(ar, {}) for ar in value])
                if key.endswith('_many_args') else
                # add_args -> add(*value)
                (key[:-5], [(value, {})])
                )
            if key.endswith('_args') else
            (
                # add_many_kwargs -> add(**value[0]), add(**value[1]), ...
                (key[:-12], [((), kw) for kw in value])
                if key.endswith('_many_kwargs') else
                # add_kwargs -> add(**value)
                (key[:-7], [((), value)])
                )
            if key.endswith('_kwargs') else
            # add_many -> add(value[0]), add(value[1]), ...
            (key[:-5], [((v,), {}) for v in value])
            if key.endswith('_many') else
            # add -> add(value)
            (key, [((value,), {})])
            )

        fnc = (
            getattr(obj, key, None)  # add -> add
            or getattr(obj, f'set_{key}', None)  # range -> set_range
            or getattr(obj, key)  # raise AttributeError for key
            )

        for ar, kw in calls:
            fnc(*ar, **kw)
            register((*ar, *kw.values()))

        register((obj,))

    register((self,))

    return self


def wrap(cls: type[G], *args, **defaults) -> typing.Callable[..., G]:
    """Wrap given GObject into a factory with extra option handling."""
    return functools.partial(create, cls, *args, **defaults)
