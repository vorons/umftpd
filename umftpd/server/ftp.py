"""FTP server classes and network utilities."""

import asyncio
import functools
import typing

import pyftpdlib.authorizers
import pyftpdlib.handlers
import pyftpdlib.servers

import umftpd.config as cfg
import umftpd.server as srv

BANNER = '{}-{} ready.'.format(
    cfg.metadata['name'],
    cfg.metadata['version'],
    )


class Permissions:

    CWD = 'e'
    CDUP = CWD
    LIST = 'l'
    NLST = LIST
    STAT = LIST
    MLSD = LIST
    MLST = LIST
    SIZE = LIST
    RETR = 'r'
    APPE = 'a'
    DELE = 'd'
    RMD = DELE
    RNFR = 'f'
    RNTO = RNFR
    MKD = 'm'
    STOR = 'w'
    STOU = STOR
    SITE_CHMOD = 'M'
    SITE_MFMT = 'T'

    R = CWD + LIST + RETR
    A = APPE + MKD + STOR + SITE_MFMT
    W = A + DELE + RNFR + SITE_CHMOD
    RW = R + W


class Authorizer(pyftpdlib.authorizers.DummyAuthorizer):

    def __init__(self, server: srv.Server) -> None:
        super().__init__()

        self.auth = server.config.auth
        self.add_user(
            server.config.auth.username,
            '',
            server.config.auth.home,
            perm=Permissions.R if server.readonly else Permissions.RW,
            )

    def validate_authentication(
            self,
            username: str,
            password: str,
            handler: typing.Any,
            ) -> None:
        if username in self.user_table and self.auth(username, password):
            return
        raise pyftpdlib.authorizers.AuthenticationFailed


class FTPHandler(pyftpdlib.handlers.FTPHandler):

    banner = BANNER
    manager: 'FTPServer'
    authorizer: 'Authorizer'

    def _emit(self, name: str, *args) -> None:
        if self.authenticated:
            self.manager.emit(name, *args)

    _partial = functools.partialmethod
    on_disconnect = _partial(_emit, 'disconnect')
    on_login = _partial(_emit, 'session')
    on_file_sent = _partial(_emit, 'download')
    on_file_received = _partial(_emit, 'upload')
    on_incomplete_file_sent = _partial(_emit, 'download')
    on_incomplete_file_received = _partial(_emit, 'upload')


class FTPSHandler(pyftpdlib.handlers.TLS_FTPHandler, FTPHandler):
    pass


class FTPServer(srv.Server):

    backend: pyftpdlib.servers.FTPServer

    async def setup(self) -> None:
        secure = self.config.secure == cfg.SECURE_MODE_SECURE
        base = (FTPHandler, FTPSHandler)[secure]
        certs = {
            'keyfile': str(self.config.ssl.keyfile),
            'certfile': str(self.config.ssl.certfile),
            } if secure else {}
        self.backend = await asyncio.to_thread(
            pyftpdlib.servers.FTPServer,
            address_or_socket=('', self.config.port),
            handler=type(f'{self!r}:{base.__name__}', (base,), {
                'authorizer': Authorizer(self),
                'manager': self,
                **certs,
                }),
            backlog=srv.BACKLOG,
            )

    async def mainloop(self) -> None:
        def mainloop() -> None:
            try:
                while True:
                    iterate()
            except StopIteration:
                pass
            finally:
                self.backend.close_all()

        def stop() -> typing.NoReturn:
            raise StopIteration

        iterate = functools.partial(
            self.backend.ioloop.loop,
            self.timeout,
            False,
            )
        task = asyncio.create_task(
            asyncio.to_thread(mainloop),
            name=f'{type(self).__name__}:mainloop',
            )
        try:
            await asyncio.shield(task)
        finally:
            iterate = stop
            await asyncio.wait_for(task, self.timeout)
