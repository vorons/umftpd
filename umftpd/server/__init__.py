"""Network utilities."""

import abc
import asyncio
import contextlib
import ipaddress
import os
import socket

import ifaddr
import zeroconf
import zeroconf.asyncio

import umftpd.config as cfg

from gi.repository import Gio, GLib

MIN_PORT = 1024 if cfg.UID else 1
MAX_PORT = 65535
BACKLOG = 32


class Application(abc.ABC):
    config: cfg.Config

    @abc.abstractmethod
    def activate_action(self, name: str, data: GLib.Variant | None) -> None:
        pass


class Server:

    actions = tuple(
        Gio.SimpleAction(
            name=name,
            parameter_type=vartype and GLib.VariantType(vartype),
            enabled=True,
            )
        for name, vartype in (
            ('server:init', None),
            ('server:start', None),
            ('server:stop', None),
            ('server:session', 's'),
            ('server:disconnect', None),
            ('server:upload', 's'),
            ('server:download', 's'),
            )
        )

    @property
    def config(self) -> cfg.Config:
        return self.application.config

    @property
    def readonly(self) -> bool:
        return (
            self.config.readonly
            or not os.access(self.config.directory, os.W_OK)
            )

    @property
    def services(self) -> list[zeroconf.ServiceInfo]:
        summary = cfg.metadata['summary']
        return [
            zeroconf.ServiceInfo(
                f'_{protocol}._tcp.local.',
                f'{summary}._{protocol}._tcp.local.',
                port=self.application.config.port,
                server=f'{socket.gethostname()}.local.',
                )
            for protocol in self.application.config.protocols
            ]

    def __init__(self, application: Application, timeout: float) -> None:
        self.application = application
        self.timeout = timeout

    def emit(self, name: str, argument: str | None = None) -> None:
        GLib.idle_add(
            self.application.activate_action,
            f'server:{name}',
            argument and GLib.Variant.new_string(argument),
            )

    @abc.abstractmethod
    async def setup(self) -> None:
        pass

    @abc.abstractmethod
    async def mainloop(self) -> None:
        pass

    @classmethod
    async def run(cls, *args, **kwargs) -> None:
        self = cls(*args, **kwargs)
        self.emit('init')
        await self.setup()
        self.emit('start')
        try:
            async with zeroconf.asyncio.AsyncZeroconf() as zc:
                await asyncio.gather(
                    self.mainloop(),
                    *map(zc.async_register_service, self.services),
                    )
        finally:
            self.emit('stop')


def check_port(port: int, ipv6: bool = False) -> bool:
    """Check if port is available."""
    family = socket.AF_INET6 if ipv6 else socket.AF_INET
    sock = socket.socket(family, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    with contextlib.closing(sock), contextlib.suppress(OSError):
        sock.bind(('', port))
        return True
    return False


def route_host() -> list[str]:
    """Get network addresses to current host."""
    addresses = [
        str(address)
        for adapter in ifaddr.get_adapters()
        for ip in adapter.ips
        if (address := (
            ipaddress.IPv4Address(ip.ip) if ip.is_IPv4 else
            ipaddress.IPv6Address(ip.ip[0]) if ip.is_IPv6 else
            None
            )) and not address.is_loopback
        ]
    return [*addresses, f'{socket.gethostname()}.local'] if addresses else []
