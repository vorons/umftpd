"""umftpd - Usermode FTP Server."""

import asyncio
import collections
import collections.abc
import concurrent.futures
import contextlib
import datetime
import functools
import itertools
import logging
import os
import pathlib
import re
import time
import typing
import urllib.parse

import umftpd.aio as aio
import umftpd.config as cfg
import umftpd.crypto as crypto
import umftpd.server as srv
import umftpd.server.ftp as ftp
import umftpd.server.ssh as ssh
import umftpd.ui as ui
import umftpd.uitk as uitk

from gi.repository import Adw, Gdk, Gio, GLib, Gtk


__author__ = 'Felipe A Hernandez <ergoithz@gmail.com>'


logger = logging.getLogger(__name__)

T = typing.TypeVar('T')

RE_MARKUP = re.compile('<[^>]+>')


class GLibLogHandler(logging.Handler):
    def __init__(
            self,
            callback: typing.Callable[[logging.LogRecord], typing.Any],
            ) -> None:
        super().__init__()
        self.callback = callback

    def emit(self, record: logging.LogRecord) -> None:
        GLib.idle_add(self.callback, record)


class Application(uitk.UiApplication):

    application_id = 'eu.ithz.umftpd'

    window: Adw.ApplicationWindow | None = None
    window_size_request: tuple[int, int] | None = None
    skip_network_check: bool = False

    initialized: bool = False
    stopping: bool = False

    port_valid: bool = True
    hosts: tuple[str] = ()

    server: concurrent.futures.Future = aio.RESOLVED
    server_timeout: float = 0.5
    server_error_flush: float = 0
    server_statuses = 'starting', 'online', 'offline'

    status_time: float = 0

    actions = srv.Server.actions
    protocol_reverse_cycling: bool = False
    protocols = 'ftp', 'ftps', 'sftp'

    loggers = f'{__name__}:application', 'pyftpdlib', 'asyncssh'

    @property
    def server_class(self) -> type[srv.Server]:
        return (
            ssh.SFTPServer if self.config.secure == cfg.SECURE_MODE_SSH else
            ftp.FTPServer
            )

    @property
    def server_addresses(self) -> tuple[str]:
        username = self.config.auth.username
        port = self.config.port
        return tuple(
            f'{protocol}://{username}@{host}:{port}'
            for protocol in self.config.protocols
            for host in (f'[{h}]' if ':' in h else h for h in self.hosts)
            )

    @property
    def server_type(self) -> str:
        protocol = self.protocols[self.config.secure]
        return ui.main[f'protocol_switch_{protocol}'].get_label()

    @property
    def home(self) -> str:
        path = self.config.directory
        return (
            cfg.USER_HOME.as_posix()
            if path == cfg.USER_HOME else
            f'~/{path.relative_to(cfg.USER_HOME).as_posix()}'
            if path.is_relative_to(cfg.USER_HOME) else
            path.absolute().as_posix()
            )

    @functools.cached_property
    def urls(self) -> collections.abc.Mapping[str, str]:
        return dict(
            item.split(', ', 1)
            for item in cfg.metadata.get_all('Project-URL')
            if ', ' in item
            )

    @functools.cached_property
    def config(self) -> cfg.Config:
        return cfg.Config.from_environment()

    @functools.cached_property
    def user_directories(self) -> tuple[tuple[pathlib.Path, str, str], ...]:
        theme = Gtk.IconTheme.get_for_display(ui.main.root.get_display())
        home = pathlib.Path.home()
        root = pathlib.Path(home.anchor)
        special_dirs = (
            (pathlib.Path(path), key[10:].replace('_', '').lower())
            for key, path in (
                (key, GLib.get_user_special_dir(value))
                for key, value in vars(GLib.UserDirectory).items()
                if key.startswith('DIRECTORY_')
                )
            if path
            )
        return (
            (home, ui.strings.directory_home, 'user-home'),
            (root, ui.strings.directory_computer, 'drive-harddisk'),
            *(
                (path, path.name, next(
                    filter(theme.has_icon, (f'folder-{slug}', f'user-{slug}')),
                    'folder',
                    ))
                for path, slug in special_dirs
                ),
            )

    @ui.app.connect('handle-local-options')
    def on_local_options(
            self,
            app: 'Application',
            options: GLib.VariantDict,
            ) -> typing.Literal[-1]:
        if size := options.lookup_value('window-size', GLib.VariantType('s')):
            w, h = map(int, size.get_string().split('x'))
            self.window_size_request = w, h
        self.skip_network_check = bool(
            options.lookup_value('localhost', GLib.VariantType('b')),
            )
        return -1

    @ui.app.connect('startup')
    def on_startup(self, app: 'Application') -> None:
        self.initialized = True
        self.aioloop.start()

    @ui.app.connect('activate')
    def on_activate(self, app: 'Application') -> None:
        if self.window_size_request:
            ui.main.root.set_default_size(*self.window_size_request)
        ui.main.root.present()

    @ui.app.connect('shutdown')
    def on_shutdown(self, app: 'Application') -> None:
        self.stopping = True
        self.aioloop.stop(self.server_timeout)

    @ui.app.connect(
        'server:session', 'activate', None,
        'server_active_sessions', 1,
        )
    @ui.app.connect(
        'server:disconnect', 'activate', None,
        'server_active_sessions', -1,
        )
    @ui.app.connect(
        'server:upload', 'activate', None,
        'server_total_uploads', 1,
        )
    @ui.app.connect(
        'server:download', 'activate', None,
        'server_total_downloads', 1,
        )
    def on_server_stat(
            self,
            action: Gio.Action, data: GLib.Variant,
            target: str, change: int,
            ) -> None:
        self.server_stats[target] += change
        text = ui.strings.render(target, num=self.server_stats[target])
        ui.main[target].set_label(text)

    @ui.app.connect('server:start', 'activate')
    def on_server_start(self, actio: Gio.Action, data: GLib.Variant) -> None:
        message = ui.strings.render_server_log_info(
            config=self.config,
            home=self.home,
            addresses=self.server_addresses,
            )
        self.logger.info(
            RE_MARKUP.sub('', message),
            extra={'markup': message, 'scroll': True},
            )
        GLib.idle_add(self.set_status, 'online')
        GLib.idle_add(
            ui.main.status_page.set_icon_name,
            ui.STATUS_ICON_CONNECTED,
            )

    @ui.about.connect('close-request')
    @ui.directory_error.connect('response')
    @ui.directory_root_error.connect('response')
    def on_dialog(self, widget: Gtk.Dialog, res: typing.Any = None) -> None:
        widget.hide()

    @ui.directory.connect('response')
    def on_directory(
            self,
            widget: Gtk.FileChooserNative,
            res: Gtk.ResponseType,
            ) -> None:
        if res == Gtk.ResponseType.ACCEPT:
            selected = widget.get_file()
            if not selected:
                # WORKAROUND(flatpak bug): show error dialog
                # https://github.com/flatpak/xdg-desktop-portal/issues/820
                ui.directory_error.root.show()
                return
            path = selected.get_path()
            if cfg.FLATPAK and path == '/':
                # WORKAROUND(flatpak/fuse limitation): show error dialog
                # fuse cannot mount root, so flatpak returns its internal root
                ui.directory_root_error.root.show()
                return
            self.set_directory(path, True)

    @ui.main.connect('help', 'activate')
    def on_help(self, action: Gio.Action, data: None) -> None:
        Gtk.show_uri(self.window, self.urls['help'], Gdk.CURRENT_TIME)

    @ui.main.connect('known-issues', 'activate')
    def on_issues(self, action: Gio.Action, data: None) -> None:
        Gtk.show_uri(self.window, self.urls['known-issues'], Gdk.CURRENT_TIME)

    @ui.main.connect('about', 'activate')
    def on_about(self, action: Gio.Action, data: None) -> None:
        ui.about.root.present()

    @ui.main.connect('user_reset', 'clicked', None, 'username')
    @ui.main.connect('password_reset', 'clicked', None, 'password')
    @ui.main.connect('directory_reset', 'clicked', None, 'directory')
    @ui.main.connect('port_reset', 'clicked', None, 'port')
    def on_reset(self, widget: Gtk.Button, value: str) -> None:
        getattr(self, f'set_{value}')(None, True)

    @ui.main.connect('share_email', 'clicked', None,
                     'mailto:?subject={summary}&body={body}')
    @ui.main.connect('share_telegram', 'clicked', None,
                     'https://t.me/share/url?text={message}&url={address}')
    @ui.main.connect('share_whatsapp', 'clicked', None,
                     'https://api.whatsapp.com/send?text={message}+{address}')
    def on_share(self, widget: Gtk.Button, uri: str) -> None:
        message = ui.strings.render_share_address_message(
            type=self.server_type,
            )
        summary = ui.strings.render_share_address_summary(
            type=self.server_type,
            )
        address = self.server_addresses[0]
        raw = {
            'address': address,
            'message': message,
            'summary': summary,
            'body': f'{address}\n\n{message}',
            }
        context = {k: urllib.parse.quote_plus(v) for k, v in raw.items()}
        Gtk.show_uri(self.window, uri.format(**context), Gdk.CURRENT_TIME)

    @ui.main.connect('directory_button', 'clicked')
    def on_directory_click(self, widget: Gtk.Button) -> None:
        ui.directory.root.show()

    @ui.main.connect('readonly_switch', 'state-set')
    def on_readonly_change(self, widget: Gtk.Switch, value: bool) -> None:
        self.set_readonly(value, self.initialized)

    @ui.main.connect('port_entry', 'value-changed')
    def on_port_change(self, widget: Gtk.SpinButton) -> None:
        self.set_port(widget.get_value_as_int(), self.initialized)

    @ui.main.connect('logs_toggle', 'clicked')
    def on_logs_toggle(self, widget: Gtk.ToggleButton) -> None:
        self.set_logs_visible(widget.get_active(), True)

    @ui.main.connect('address_toggle', 'clicked')
    def on_address_toggle(self, widget: Gtk.ToggleButton) -> None:
        active = widget.get_active()
        widget.set_icon_name(ui.REVEAL_END_ICONS[active])
        ui.main.address_revealer.set_reveal_child(active)

    @ui.main.connect('protocol_switch_ftp', 'clicked', None, 0)
    @ui.main.connect('protocol_switch_ftps', 'clicked', None, 1)
    @ui.main.connect('protocol_switch_sftp', 'clicked', None, 2)
    def on_protocol_change(self, widget: Gtk.ToggleButton, value: int) -> None:
        self.set_protocol(value, True)

    @ui.main.connect('protocol_row', 'activated')
    def on_protocol_click(self, widget: Gtk.Widget) -> None:
        inc = (1, -1)[self.protocol_reverse_cycling]
        self.set_protocol((self.config.secure + inc) % len(self.protocols))

    @ui.main.connect('server_start', 'activated', None, True)
    @ui.main.connect('server_stop', 'clicked', None, False)
    def on_server_click(self, widget: Gtk.Widget, value: bool) -> None:
        self.set_online(value, True)

    def set_username(self, username: str, user: bool = False) -> None:
        entry: Gtk.Entry = ui.main.user_entry
        entry.set_text('')  # TODO(if made active): make all this conditional
        entry.get_first_child().set_placeholder_text((
            self.config.default.username if user or not username else
            username
            ))

        if user:
            self.config = self.config.with_updates(username=username)

    def set_password(self, password: typing.Any, user: bool = False) -> None:
        entry: Gtk.PasswordEntry = ui.main.password_entry
        entry.set_text('')  # TODO(if made active): make all this conditional
        entry.get_first_child().set_placeholder_text((
            self.config.default.password
            if user else
            ui.strings._dummy_password
            if isinstance(password, crypto.CryptoHash) else
            password
            ))

        if user:
            self.config = self.config.with_updates(password=password)

    def set_port(self, port: int | None, user: bool = False) -> None:
        entry: Gtk.SpinButton = ui.main.port_entry

        if port is None:
            port = self.config.default.port

        if port != entry.get_value_as_int():
            entry.set_value(port)
            if user:
                return  # wait for value-changed

        if self.check_port(port) and user:
            self.config = self.config.with_updates(port=port)

    def set_directory(
            self,
            path: os.PathLike | None,
            user: bool = False,
            ) -> None:
        path = pathlib.Path(path) if path else self.config.default.directory

        if user:
            self.config = self.config.with_updates(directory=path)
            self.validate_directory()

        ui.main.directory_button.set_tooltip_markup(
            ui.strings._render_directory_tooltip(home=self.home),
            )

    def set_protocol(self, value: int, user: bool = False) -> None:
        protocol = self.protocols[value]
        button: Gtk.ToggleButton = ui.main[f'protocol_switch_{protocol}']
        if not button.get_active():
            button.set_active(True)
            if user:
                return  # wait for value-changed

        if value in (0, len(self.protocols) - 1):
            self.protocol_reverse_cycling = bool(value)

        for i, proto in enumerate(self.protocols):
            ui.main[f'protocol_{proto}_label'].set_visible(i == value)

        if self.config.secure != value:
            self.config = self.config.with_updates(secure=value)

    def set_readonly(self, readonly: bool, user: bool = False) -> None:
        switch: Gtk.Switch = ui.main.readonly_switch

        if readonly != switch.get_active():
            switch.set_active(readonly)
            if user:
                return  # wait for value-changed

        if user:
            self.config = self.config.with_updates(readonly=readonly)
            ui.main.readonly_disabled.set_reveal_child(not readonly)

    def set_logs_visible(self, state: bool, user: bool = False) -> None:
        revealer = ui.main.logs_revealer
        toggle = ui.main.logs_toggle
        page = ui.main.status_page
        tooltips = ui.strings.server_logs_show, ui.strings.server_logs_hide

        if state != toggle.get_active():
            toggle.set_active(state)

        page.set_vexpand(not state)
        revealer.set_visible(state)
        toggle.set_icon_name(ui.REVEAL_UP_ICONS[state])
        toggle.set_tooltip_text(tooltips[state])

        if state:
            GLib.idle_add(revealer.set_reveal_child, True)
            GLib.idle_add(self.scroll_logs_to_bottom)
        else:
            revealer.set_reveal_child(False)

        if user:
            self.set_config(ui={'logs': state})

    def set_server_error(self, error: str | None = None) -> None:
        row = ui.main.server_start
        revealer = ui.main.server_error_revealer

        if error:
            self.server_error_flush = time.time() + 1 + (
                revealer.get_transition_duration() / 1000.
                )
            ui.main.server_error_label.set_label(error)

        (row.add_css_class if error else row.remove_css_class)('error')
        revealer.set_reveal_child(error is not None)

    def set_online(self, state: bool, user: bool = False) -> None:
        if state:
            self.hosts = srv.route_host()
            if not self.validate_network():
                return

            self.set_status('starting')
            self.set_server_error()

        pages = ui.main.page_left, ui.main.page_right
        transitions = (
            Gtk.StackTransitionType.SLIDE_RIGHT,
            Gtk.StackTransitionType.SLIDE_LEFT,
            )

        ui.main.back_revealer.set_reveal_child(state)
        ui.main.stack.set_transition_type(transitions[state])
        ui.main.stack.set_visible_child(pages[state])

        if not state:
            self.aioloop.cancel(self.server)
            return

        ui.main.server_type.set_label(ui.strings.render_server_status_type(
            type=self.server_type,
            ))

        for target in self.server_stats:
            ui.main[target].set_label(ui.strings.render(target, num=0))
        self.server_stats.clear()

        self.set_config(**{
            key: value
            for key, value in (
                # update values from passive fields
                ('username', ui.main.user_entry.get_text()),
                ('password', ui.main.password_entry.get_text()),
                )
            if value
            })

        primary, *extra = self.server_addresses or ('',)
        addrbox = ui.main.address_box
        ui.main.address_label.set_visible(bool(primary))
        ui.main.address_label.set_label(primary)
        ui.main.address_toggle.set_visible(bool(extra))
        ui.main.address_revealer.set_visible(bool(extra))
        for address, widget in itertools.zip_longest(extra, tuple(addrbox)):
            if address and widget:
                widget.set_label(address)
            elif address:
                addrbox.append(ui.UiAddressLabel(label=address))
            else:
                addrbox.remove(widget)

        ui.main.status_page.set_icon_name(ui.STATUS_ICON_DISCONNECTED)

        self.server = self.aioloop.submit(self.run_server())

    def set_status(self, status: str, created: float | None = None) -> None:
        if created and created < self.status_time:
            return
        self.status_time = time.time()

        for name in self.server_statuses:
            ui.main[f'server_status_{name}'].set_visible(name == status)

    def set_config(self, **updates) -> cfg.Config:
        self.config = config = self.config.with_updates(
            info={
                **updates.pop('info', {}),
                'last': datetime.datetime.now(tz=cfg.TZ),
                },
            **updates,
            )
        config.save()
        return config

    def validate_network(self) -> bool:
        if not self.hosts and not self.skip_network_check:
            self.set_server_error(ui.strings.server_error_no_host)
            logger.warning('Server start bounced, no route to host')
            return False

        if not self.server.done():
            self.set_server_error(ui.strings.server_error_running)
            logger.warning('Server start bounced, already running')
            return False

        if not (self.port_valid and self.check_port(self.config.port)):
            ui.main.port_entry.grab_focus()
            logger.warning('Server start bounced, invalid port')
            return False

        if not self.validate_directory():
            ui.main.directory_button.grab_focus()
            return False

        return True

    def validate_directory(self) -> bool:
        label, icon, readable, writable = self.identify_directory(
            self.config.directory,
            )
        ui.main.directory_content.set_label(label)
        ui.main.directory_content.set_icon_name(icon)
        ui.main.readonly_disabled_label.set_css_classes([(
            'warning' if writable else
            'dim-label' if readable else
            'error'
            )])
        ui.main.readonly_disabled_label.set_label((
            ui.strings.readonly_disabled_warning if writable else
            ui.strings.readonly_directory_warning if readable else
            ui.strings.invalid_directory_warning
            ))
        ui.main.readonly_disabled.set_reveal_child((
            not writable
            or not self.config.readonly
            ))
        ui.main.readonly_switch.set_visible(writable)
        return readable

    def identify_directory(self,
                           path: pathlib.Path,
                           ) -> tuple[str, str, bool, bool]:
        if path.is_dir() and not (cfg.FLATPAK and path.parent is path):
            writable = os.access(path, os.W_OK | os.R_OK | os.X_OK)
            readable = writable or os.access(path, os.R_OK | os.X_OK)
            suppress_not_found = contextlib.suppress(FileNotFoundError)
            for location, name, icon in self.user_directories:
                with suppress_not_found:
                    if location.samefile(path):
                        return name, icon, readable, writable
            return path.name, 'folder', readable, writable
        return path.name, 'action-unavailable-symbolic', False, False

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, application_id=self.application_id, **kwargs)

        self.aioloop = aio.AIOLoopThread()

        self.get_style_manager().set_color_scheme(Adw.ColorScheme.PREFER_DARK)
        self.set_accels_for_action('app.help', ['F1'])
        self.add_main_option(
            'window-size',
            ord('s'),  # WTF GObject!
            0,
            GLib.OptionArg.STRING,
            'Desired window size',
            'WIDTHxHEIGHT',
            )
        self.add_main_option(
            'localhost',
            0,
            0,
            GLib.OptionArg.NONE,
            'Disable network check',
            )

        self.server_stats = collections.defaultdict(int)

        self.textbuffer: Gtk.TextBuffer = ui.main.textview.get_buffer()
        self.textend = self.textbuffer.get_end_iter()
        self.textmark = self.textbuffer.create_mark('', self.textend, False)

        ui.main.port_entry.set_range(srv.MIN_PORT, srv.MAX_PORT)

        self.set_username(self.config.username)
        self.set_password((
            self.config.password
            or self.config.password_hash
            or self.config.default.password
            ))
        self.set_directory(self.config.directory)
        self.set_readonly(self.config.readonly)
        self.set_port(self.config.port)
        self.set_protocol(self.config.secure)
        self.set_logs_visible(self.config.ui.logs)
        self.validate_directory()

        handler = GLibLogHandler(self.on_log_record)

        for name in self.loggers:
            logger = logging.getLogger(name)
            logger.addHandler(handler)
            logger.setLevel(logging.INFO)

        self.logger = logging.getLogger(self.loggers[0])

    def on_log_record(self, record: logging.LogRecord) -> None:
        created = datetime.datetime.fromtimestamp(record.created, tz=cfg.TZ)
        message = '<tt>{} {} </tt>{}\n'.format(
            created.replace(microsecond=0).isoformat(' '),
            record.levelname,
            (
                getattr(record, 'markup', None)
                or f'<b>{GLib.markup_escape_text(record.getMessage())}</b>'
                ),
            )

        textview: Gtk.TextView = ui.main.textview
        adjustment: Gtk.Adjustment = textview.get_vadjustment()
        bottomed = getattr(record, 'scroll', None) or (
            adjustment.get_upper()
            - adjustment.get_value()
            - adjustment.get_page_size()
            - textview.get_top_margin()
            - textview.get_bottom_margin()
            ) <= 1
        self.textbuffer.insert_markup(self.textend, message, -1)

        if bottomed:
            GLib.idle_add(self.scroll_logs_to_bottom)

    def scroll_logs_to_bottom(self) -> None:
        ui.main.textview.scroll_to_mark(self.textmark, 0, False, 0, 0)

    def check_port(self, port: int) -> bool:
        self.port_valid = valid = (
            srv.check_port(port, False)
            and srv.check_port(port, True)
            )
        box: Gtk.Box = ui.main.port_box
        (box.remove_css_class if valid else box.add_css_class)('error')

        if not valid:
            ui.main.port_error.set_label(ui.strings.port_unavailable_warning)

        ui.main.port_error_revealer.set_reveal_child(not valid)
        return valid

    async def run_server(self) -> None:
        try:
            await self.server_class.run(self, self.server_timeout)
        except asyncio.CancelledError:
            pass
        except crypto.SSLCertificateError as exc:
            self.logger.exception('Certificate error: %s', *exc.args)
        except Exception:
            self.logger.exception('%s error', self.server_class.__name__)

        if not self.stopping:
            GLib.timeout_add(
                int(max(0, self.server_error_flush - time.time()) * 1000),
                self.set_server_error,
                )
            GLib.idle_add(self.set_status, 'offline')

        self.logger.info(ui.strings.server_log_stopped)
