"""Module script entrypoint."""

import logging
import sys
import typing

import umftpd

if typing.TYPE_CHECKING:
    import collections.abc


def main(
        argv: 'collections.abc.Sequence[str] | None' = None,
        ) -> typing.NoReturn:
    """Run application."""
    logging.basicConfig()
    app = umftpd.Application()
    sys.exit(app.run(sys.argv if argv is None else argv))


if __name__ == '__main__':
    main()
